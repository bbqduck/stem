﻿/* Note: due to repid change and modification of the logic section
 * some repeated code is not optimized
 * 
 * 
 */
namespace Comm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data;

    public class Stock
    {
        // settings
        public const double _range = 0.6; //percent
        // variables
        string _name { get; set; }
        public DataTable _data { get; set; }
        public DataTable _graph { get; set; }
        public DataTable _window { get; set; }

        public Stock()
        {
            //Initialize variables

        }

        public Stock(string name)
        {
            _name = name;
        }

        //data format:
        //Date	        Time	    Open	High	Low	    Close	Volume	OpenInt
        //07/07/2015	15:35:00	523.13	524	    521.235	521.42	21075	0

        //assuming const time interval even between days: 5 min
        public void BuildGraph()
        {
            if (_data == null || _data.Rows.Count < 1) return;
            //can add checking for existing
            const string indexCol = "count";
            const string priceCol = "price";
            const string openCol = "Open";
            const string closeCol = "Close";

            _graph = new DataTable { TableName = _name };
            _graph.Columns.Add(indexCol);
            _graph.Columns.Add(priceCol);

            foreach (DataRow row in _data.Rows)
            {

                if (_graph.Rows.Count < 1)
                {
                    var r = _graph.NewRow();
                    r[indexCol] = 0;
                    r[priceCol] = row[openCol];
                    _graph.Rows.Add(r);
                }
                else
                {
                    //update old r with new open from row
                    var r = _graph.Rows[_graph.Rows.Count - 1];
                    var oldClosingPrice = Convert.ToDouble(r[priceCol]);
                    var newOpeningPrice = Convert.ToDouble(row[openCol]);
                    r[priceCol] = GetAverage(new List<double>() { oldClosingPrice, newOpeningPrice });
                }
                //add new entry with closing info
                var entry = _graph.NewRow();
                entry[indexCol] = _graph.Rows.Count;
                entry[priceCol] = row[closeCol];
                _graph.Rows.Add(entry);
            }
        }

        //process _graph into valuable window of interest
        //Define Window: consists of start point and end point
        //Define point: consists of index and price
        public void BuildWindows()
        {
            if (_graph == null || _graph.Rows.Count < 1) return;
            // initialize windows 
            _window = new DataTable { TableName = "windows" };
            const string startIndexCol = "Start Index";
            const string endIndexCol = "End Index";
            const string startPriceCol = "Start Price";
            const string endPriceCol = "End Price";

            _window.Columns.Add(startIndexCol);
            _window.Columns.Add(startPriceCol);
            _window.Columns.Add(endIndexCol);
            _window.Columns.Add(endPriceCol);

            // going through data by entry
            for (var i = 0; i < _graph.Rows.Count; i++)
            {
                var entry = _graph.Rows[i];
                //process a single data entry
                ProcessEntry(_window, entry);
            }
        }

        private void ProcessEntry(DataTable _window, DataRow entry)
        {
            const string startIndexCol = "Start Index";
            const string endIndexCol = "End Index";
            const string startPriceCol = "Start Price";
            const string endPriceCol = "End Price";
            const string entryIndexCol = "count";
            const string entryPriceCol = "price";

            //regular entry
            if (_window.Rows.Count > 0)
            {
                //get latest row
                var recentWindow = _window.Rows[_window.Rows.Count - 1];
                //the new entry shuld be the next data point after the window
                //either it will be added to the existing window
                if (TryUpdateWindow(recentWindow,entry)) return;
                //or start a new window
            }
            //new window start point
            var row = _window.NewRow();
            row[startIndexCol] = entry[entryIndexCol];
            row[startPriceCol] = entry[entryPriceCol];
            row[endIndexCol] = entry[entryIndexCol];
            row[endPriceCol] = entry[entryPriceCol];
            _window.Rows.Add(row);

        }

        // return true if entry is being processed:
        // either updated the old window or data is not useful.
        // return false if entry is a start of next window
        private bool TryUpdateWindow(DataRow recentWindow, DataRow entry)
        {
            const string startIndexCol = "Start Index";
            const string endIndexCol = "End Index";
            const string startPriceCol = "Start Price";
            const string endPriceCol = "End Price";
            const string entryIndexCol = "count";
            const string entryPriceCol = "price";

            var startIndex = Convert.ToInt32(recentWindow[startIndexCol]);
            var endIndex = Convert.ToInt32(recentWindow[endIndexCol]);
            var entryIndex = Convert.ToInt32(entry[entryIndexCol]);

            var startPrice = Convert.ToDouble(recentWindow[startPriceCol]);
            var endPrice = Convert.ToDouble(recentWindow[endPriceCol]);
            var entryPrice = Convert.ToDouble(entry[entryPriceCol]);

            if (Math.Sign(entryPrice - endPrice) == Math.Sign(endPrice - startPrice))
            { 
                //extend
                recentWindow[endIndexCol] = entry[entryIndexCol];
                recentWindow[endPriceCol] = entry[entryPriceCol];
                return true;
            }
            else if (Math.Abs((endPrice - startPrice) / startPrice) * 100 > _range)
            {
                //close window, exit and start new window
                return false;
            }
            else //not closing window
            {
                //check if new extreme
                if( (entryPrice < startPrice && entryPrice < endPrice) ||
                    (entryPrice > startPrice && entryPrice > endPrice) )
                {
                    //update
                    recentWindow[startIndexCol] = recentWindow[endIndexCol];
                    recentWindow[startPriceCol] = recentWindow[endPriceCol];
                    recentWindow[endIndexCol] = entry[entryIndexCol];
                    recentWindow[endPriceCol] = entry[entryPriceCol];
                }
                //else do nothing
                return true;
            }
        }

        #region healper functions
        private double GetAverage(List<double> numbers)
        {
            return numbers.Sum() / numbers.Count;
        }

        private double GetAverage(DataTable data, string colName)
        {
            var numList = new List<double>();
            foreach (DataRow r in data.Rows)
            { 
                numList.Add(Convert.ToDouble(r[colName]));
            }
            return GetAverage(numList);
        }

        private double GetMax(DataTable data, string colName)
        { 
            var numList = new List<double>();
            foreach (DataRow r in data.Rows)
            { 
                numList.Add(Convert.ToDouble(r[colName]));
            }
            return numList.Max();
        }

        private double GetMin(DataTable data, string colName)
        { 
            var numList = new List<double>();
            foreach (DataRow r in data.Rows)
            { 
                numList.Add(Convert.ToDouble(r[colName]));
            }
            return numList.Min();
        }

        public double GetDeltaInPercent(DataTable data, string colName)
        { 
            var numList = new List<double>();
            foreach (DataRow r in data.Rows)
            { 
                numList.Add(Convert.ToDouble(r[colName]));
            }
            var average = GetAverage(numList);
            var max = numList.Max();
            var min = numList.Min();

            return Math.Abs(max - min) / average * 100;
        }
        #endregion
    }
}
