﻿namespace ExcelMapperTests
{
    using ExcelMapper;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Data;
    using TxtDataReader;
    using Comm;
    

    [TestClass]
    public class ExcelWriterTests
    {
        [TestMethod]
        public void WriteDataTableTest()
        {
            // arrange
            var filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\TxtDataReaderTests\goog.us.txt";
            // act
            var reader = new TxtDataReader();
            reader.ReadTxtFile(filePath);
            var data = reader.GetDataTable();
            // assert
            var count = 0;
            var colName = "Date";
            foreach (DataRow row in data.Rows)
            { 
                if((string)row[colName] =="2015-07-23") count++;
            }
            //Assert.AreEqual(78, count, 0, "count error, get count of: "+ count );
            filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\ExcelMapperTests\goog.us.xlsx";
            var writer = new ExcelWriter();
            Assert.IsTrue(writer.WriteDataTable(filePath, data, true),"File output fail.");
        }

        [TestMethod]
        public void BuildGraphTest()
        {
            // arrange
            var filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\TxtDataReaderTests\goog.us.txt";
            // act
            var reader = new TxtDataReader();
            reader.ReadTxtFile(filePath);
            var data = reader.GetDataTable();
            // assert
            var count = 0;
            var colName = "Date";
            foreach (DataRow row in data.Rows)
            {
                if ((string)row[colName] == "2015-07-23") count++;
            }
            //Assert.AreEqual(78, count, 0, "count error, get count of: "+ count );

            var goog = new Stock("goog");
            goog._data = data;
            goog.BuildGraph();

            filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\ExcelMapperTests\Graph_goog.us.xlsx";
            var writer = new ExcelWriter();
            Assert.IsTrue(writer.WriteDataTable(filePath, goog._graph, true), "File output fail.");
        }

        [TestMethod]
        public void GetDeltaInPercentTest()
        {
            // arrange
            var filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\TxtDataReaderTests\goog.us.txt";
            // act
            var reader = new TxtDataReader();
            reader.ReadTxtFile(filePath);
            var data = reader.GetDataTable();
            // assert
            var count = 0;
            var colName = "Date";
            foreach (DataRow row in data.Rows)
            {
                if ((string)row[colName] == "2015-07-23") count++;
            }
            //Assert.AreEqual(78, count, 0, "count error, get count of: "+ count );

            var goog = new Stock("goog");
            goog._data = data;
            goog.BuildGraph();
            var delta = goog.GetDeltaInPercent(goog._graph, "price");
            Assert.AreEqual(27, delta, 1, "delta not in range " + delta);
        }

        [TestMethod]
        public void BuildWindowsTest()
        {
            // arrange
            var filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\TxtDataReaderTests\goog.us.txt";
            // act
            var reader = new TxtDataReader();
            reader.ReadTxtFile(filePath);
            var data = reader.GetDataTable();
            // assert
            var count = 0;
            var colName = "Date";
            foreach (DataRow row in data.Rows)
            {
                if ((string)row[colName] == "2015-07-23") count++;
            }
            //Assert.AreEqual(78, count, 0, "count error, get count of: "+ count );

            var goog = new Stock("goog");
            goog._data = data;
            goog.BuildGraph();
            var delta = goog.GetDeltaInPercent(goog._graph, "price");
            //Assert.AreEqual(27, delta, 1, "delta not in range " + delta);

            goog.BuildWindows();
            filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\ExcelMapperTests\Windows_goog.us.xlsx";
            var writer = new ExcelWriter();
            Assert.IsTrue(writer.WriteDataTable(filePath, goog._window, true), "File output fail.");
        }
    }
}
