﻿namespace TxtDataReaderTests
{
    using System;
    using System.Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TxtDataReader;

    [TestClass]
    public class TestTxtDataReaderTests
    {
        [TestMethod]
        public void ReadTxtFileTest()
        {
            // arrange
            var filePath = @"D:\Dev\DuckTool\DuckDoc\_testFixtures\UnitTests\MapperTests\TxtDataReaderTests\goog.us.txt";
            // act
            var reader = new TxtDataReader();
            reader.ReadTxtFile(filePath);
            var data = reader.GetDataTable();
            // assert
            var count = 0;
            var colName = "Date";
            foreach (DataRow row in data.Rows)
            { 
                if((string)row[colName] =="2015-07-23") count++;
            }
            Assert.AreEqual(78, count, 0, "count error, get count of: "+ count );
        }
    }
}
