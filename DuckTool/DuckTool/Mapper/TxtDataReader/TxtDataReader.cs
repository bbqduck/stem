﻿/*
 * Txt Data Reader, process text data into form of data table
 */
namespace TxtDataReader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data;
    using System.IO;

    public class TxtDataReader
    {
        private DataTable _dataCache { get; set; }
        private List<string> _columnIndex { get; set; }

        public DataTable GetDataTable() { return _dataCache; }

        public TxtDataReader()
        {
            _dataCache = new DataTable();
            _columnIndex = null;
        }
        
        /// <summary>
        /// Read txt file into datatable
        /// TODO:[specific structure requirement]: implement later
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public void ReadTxtFile(string filePath)
        {
            //validation
            if (!ValidFilePath(filePath)) return;
            //process file
            var lineList = new List<string>();
            StreamReader sr = new StreamReader(filePath);
            String line;
            while((line = sr.ReadLine()) != null)
            {
                lineList.Add(line);
            }
            _dataCache = new DataTable { TableName = GetFileName(filePath) };
            if(!ValidColumnIndex(lineList)) return;
            SetUpDataTableColumns(lineList[0]);
            lineList.RemoveAt(0);
            foreach (var l in lineList)
            {
                var row = _dataCache.NewRow();
                ProcessStringRow(l, row);//better not get null
                _dataCache.Rows.Add(row);
            }
        }

        private void SetUpDataTableColumns(string line)
        {
            _columnIndex = line.Split(',').ToList();
            foreach (var heading in _columnIndex)
            {
                _dataCache.Columns.Add(heading);
            }
        }

        /// <summary>
        /// check if 1st row exists for indexing
        /// </summary>
        /// <param name="lineList"></param>
        /// <returns></returns>
        private bool ValidColumnIndex(List<string> lineList)
        {
            if (lineList == null || lineList[0] == null) return false;
            return true;
        }

        private DataRow ProcessStringRow(string l, DataRow row)
        {
            var s =  l.Split(',').ToList();
            while (s.Count < _columnIndex.Count) s.Add("null");
            for(var i =0; i<s.Count;i++)
            {
                row[_columnIndex[i]] = s[i];
            }
            return row;
        }

        /// <summary>
        /// get the file name from the path string
        /// requires the file exist
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private string GetFileName(string filePath)
        {
            return Path.GetFileNameWithoutExtension(filePath);
        }

        /// <summary>
        /// check file existance
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private bool ValidFilePath(string filePath)
        {
            return File.Exists(filePath);
        }

    }
}
