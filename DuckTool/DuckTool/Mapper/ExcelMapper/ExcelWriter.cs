﻿/*
 * Writing data taqble to excel file
 * 
 */
namespace ExcelMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data;
    using System.IO;
    using Excel = Microsoft.Office.Interop.Excel;
    using System.Data.OleDb;

    public class ExcelWriter
    {
        private DataTable _dataCache { get; set; }
        private List<string> _columnIndex { get; set; }
        public DataTable GetDataTable() { return _dataCache; }

        public ExcelWriter()
        {
            _dataCache = new DataTable();
            _columnIndex = null;
        }

        public bool WriteDataTable(string filePath, DataTable data, bool replace)
        {
            if (!ValidDataTable(data)) return false;
            _dataCache = data;
            if (!ValidFilePath(filePath)) return false;
            if (File.Exists(filePath))
            {
                if (!replace) return false;
                File.Delete(filePath);
            }
            return WriteDataTableToFile(filePath);
        }


        private bool WriteDataTableToFile(string filePath)
        {
            try
            {
                // load excel, and create a new workbook
                Excel.Application excelApp = new Excel.Application();
                excelApp.Workbooks.Add();
                Excel._Worksheet workSheet = excelApp.ActiveSheet;
                // column headings
                for (int i = 0; i < _dataCache.Columns.Count; i++)
                {
                    workSheet.Cells[1, (i + 1)] = _dataCache.Columns[i].ColumnName;
                }
                workSheet.Application.ActiveWindow.SplitRow = 1;
                workSheet.Application.ActiveWindow.FreezePanes = true;
                // rows
                for (int i = 0; i < _dataCache.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (int j = 0; j < _dataCache.Columns.Count; j++)
                    {
                        workSheet.Cells[(i + 2), (j + 1)] = _dataCache.Rows[i][j];
                    }
                }
                workSheet.Columns.AutoFit();
                workSheet.SaveAs(filePath);
                excelApp.Quit();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool ValidDataTable(DataTable data)
        {
            if (data == null) return false;
            if (data.Rows.Count == 0) return false;
            return true;
        }
        private bool ValidFilePath(string filePath)
        {
            if(Directory.Exists(Path.GetDirectoryName(filePath))) return true;
            return false;
        }

    }
}