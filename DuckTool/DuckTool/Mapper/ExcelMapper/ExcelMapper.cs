﻿/*
 * Mapper functions
 */
 
namespace ExcelMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data;

    public class ExcelMapper
    {
        private DataTable _dataCache { get; set; }
        private List<string> _columnIndex { get; set; }
        public DataTable GetDataTable() { return _dataCache; }

        public ExcelMapper()
        {
            _dataCache = new DataTable();
            _columnIndex = null;
        }
    }
}
